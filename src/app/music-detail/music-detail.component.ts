import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-music-detail',
  templateUrl: './music-detail.component.html',
  styleUrls: ['./music-detail.component.css']
})
export class MusicDetailComponent implements OnInit {

  @Input() musicList: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
