import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchFileService {

  constructor(private httpClient: HttpClient) { }

  getFileData(): Observable<any> {
    return this.httpClient.get("assets/music.json");
  }
}
