import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SearchWithKeywordComponent } from './search-with-keyword/search-with-keyword.component';
import { MusicDetailComponent } from './music-detail/music-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchWithKeywordComponent,
    MusicDetailComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
