import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SearchFileService } from '../search-file.service';

import { Music, musicKeys } from '../music';

@Component({
  selector: 'app-search-with-keyword',
  templateUrl: './search-with-keyword.component.html',
  styleUrls: ['./search-with-keyword.component.css']
})
export class SearchWithKeywordComponent {

  constructor(
    private fb: FormBuilder,
    private searchService: SearchFileService
  ) { }

  result: any = [];

  fieldsToSearchIn: any;

  categories: any = [
    { id: 'title', text: 'Title' },
    { id: 'description', text: 'Description' },
    { id: 'keywords', text: 'Keywords'},
    { id: 'all', text: 'All' }
  ];

  searchForm = this.fb.group({
    searchText: ['', Validators.required],
    category: [ 'title' ]
  });

  processSearch(music: any, keyword: string) {
    let attrVal: any;
    for (let k of this.fieldsToSearchIn) {
      attrVal = music[k];
      attrVal = (typeof(attrVal) === 'object') ? this.sanitizeHtml(attrVal.join(' ')) : this.sanitizeHtml(attrVal);
      let keys = keyword.split(' ');
      if (keys.some(key => (attrVal.indexOf(key) > -1))) {
        return music;
      }
    }
  }

  onSubmit() {
    let keyword = this.searchForm.value.searchText;
    let catg = this.searchForm.value.category;
    this.fieldsToSearchIn = ( catg === 'all' ) ? musicKeys : [ catg ];
    this.searchService.getFileData().subscribe(fileData => {
      this.result = fileData.sections[0].assets.filter((music: any) =>  {
        return this.processSearch(music, keyword);
      });
    });
    
  }

  sanitizeHtml(text: string) : any {
    return text.replace(/<.*?>/g, "");
 }
}
