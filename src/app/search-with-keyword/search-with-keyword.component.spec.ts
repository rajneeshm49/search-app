import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchWithKeywordComponent } from './search-with-keyword.component';

describe('SearchWithKeywordComponent', () => {
  let component: SearchWithKeywordComponent;
  let fixture: ComponentFixture<SearchWithKeywordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchWithKeywordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchWithKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
