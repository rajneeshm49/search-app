export interface Music {
    title: string;
    description: Array<string>;
    keywords: Array<string>;
    is_asset_type_title: Boolean;
    asset_type_title: string;
    categories: Array<string>;
    order_in_category: Array<number>;
    images: object;
    supplement_information: Array<string>;
    links: Array<object>;
    
}

export const musicKeys = [
    'title',
    'description',
    'keywords',
    'asset_type_title',
    'categories',
    'order_in_category',
    'keywords',
    'supplement_information',
    'links',
];